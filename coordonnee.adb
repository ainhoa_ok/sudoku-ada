with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

package body Coordonnee is

   ---------------------------
   -- construireCoordonnees --
   ---------------------------

   function construireCoordonnees
     (ligne : Integer; colonne : Integer) return Type_Coordonnee
   is
      c : Type_Coordonnee;
   begin
      c.ligne   := ligne;
      c.colonne := colonne;
      return c;
   end construireCoordonnees;

   ------------------
   -- obtenirLigne --
   ------------------

   function obtenirLigne (c : Type_Coordonnee) return Integer is
   begin
      return c.ligne;
   end obtenirLigne;

   --------------------
   -- obtenirColonne --
   --------------------

   function obtenirColonne (c : Type_Coordonnee) return Integer is
   begin
      return c.colonne;
   end obtenirColonne;

   ------------------
   -- obtenirCarre --
   ------------------

   function obtenirCarre (c : Type_Coordonnee) return Integer is
      numCarre : Integer;
   begin
      if c.ligne > 0 and c.ligne < 4 then
         if c.colonne > 0 and c.colonne < 4 then
            numCarre := 1;
         end if;
         if c.colonne > 3 and c.colonne < 7 then
            numCarre := 2;
         end if;
         if c.colonne > 6 and c.colonne < 10 then
            numCarre := 3;
         end if;
      end if;

      if c.ligne > 3 and c.ligne < 7 then
         if c.colonne > 0 and c.colonne < 4 then
            numCarre := 4;
         end if;
         if c.colonne > 3 and c.colonne < 7 then
            numCarre := 5;
         end if;
         if c.colonne > 6 and c.colonne < 10 then
            numCarre := 6;
         end if;
      end if;

      if c.ligne > 6 and c.ligne < 10 then
         if c.colonne > 0 and c.colonne < 4 then
            numCarre := 7;
         end if;
         if c.colonne > 3 and c.colonne < 7 then
            numCarre := 8;
         end if;
         if c.colonne > 6 and c.colonne < 10 then
            numCarre := 9;
         end if;
      end if;
      return numCarre;
   end obtenirCarre;

   ----------------------------
   -- obtenirCoordonneeCarre --
   ----------------------------

   function obtenirCoordonneeCarre (numCarre : Integer) return Type_Coordonnee
   is
      c : Type_Coordonnee;
   begin
      if numCarre = 1 then
         c.ligne   := 1;
         c.colonne := 1;
      end if;
      if numCarre = 2 then
         c.ligne   := 1;
         c.colonne := 4;
      end if;
      if numCarre = 3 then
         c.ligne   := 1;
         c.colonne := 7;
      end if;

      if numCarre = 4 then
         c.ligne   := 4;
         c.colonne := 1;
      end if;
      if numCarre = 5 then
         c.ligne   := 4;
         c.colonne := 4;
      end if;
      if numCarre = 6 then
         c.ligne   := 4;
         c.colonne := 7;
      end if;

      if numCarre = 7 then
         c.ligne   := 7;
         c.colonne := 1;
      end if;
      if numCarre = 8 then
         c.ligne   := 7;
         c.colonne := 4;
      end if;
      if numCarre = 9 then
         c.ligne   := 7;
         c.colonne := 7;
      end if;

      return c;
   end obtenirCoordonneeCarre;

end Coordonnee;
